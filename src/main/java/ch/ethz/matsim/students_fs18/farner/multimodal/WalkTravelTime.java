package ch.ethz.matsim.students_fs18.farner.multimodal;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.router.util.TravelTime;
import org.matsim.vehicles.Vehicle;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class WalkTravelTime implements TravelTime {
	@Inject
	public WalkTravelTime() {

	}

	@Override
	public double getLinkTravelTime(Link link, double time, Person person, Vehicle vehicle) {
		return link.getLength() / (9.0 / 3.6);
	}
}
