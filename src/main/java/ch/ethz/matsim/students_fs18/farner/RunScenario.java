package ch.ethz.matsim.students_fs18.farner;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.contrib.multimodal.MultiModalModule;
import org.matsim.contrib.multimodal.config.MultiModalConfigGroup;
import org.matsim.contrib.multimodal.simengine.NewMultimodalModule;
import org.matsim.contrib.multimodal.tools.PrepareMultiModalScenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.run.NetworkCleaner;

import ch.ethz.matsim.baseline_scenario.BaselineModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.baseline_scenario.zurich.ZurichModule;

public class RunScenario {
	static public void main(String[] args) {
		MultiModalConfigGroup mmConfig = new MultiModalConfigGroup();
		
		Config config = ConfigUtils.loadConfig(args[0], mmConfig);

		config.travelTimeCalculator().setFilterModes(true);
		
		mmConfig.setCreateMultiModalNetwork(true);
		mmConfig.setDropNonCarRoutes(true);
		mmConfig.setMultiModalSimulationEnabled(true);
		mmConfig.setNumberOfThreads(4);
		mmConfig.setSimulatedModes("walk");

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		for (Person person : scenario.getPopulation().getPersons().values()) {
			for (Plan plan : person.getPlans()) {
				for (PlanElement element : plan.getPlanElements()) {
					if (element instanceof Leg) {
						Leg leg = (Leg) element;
						leg.setRoute(null);
					}
				}
			}
		}

		PrepareMultiModalScenario.run(scenario);
		new org.matsim.core.network.algorithms.NetworkCleaner().run(scenario.getNetwork());
		
		System.exit(1);

		Controler controler = new Controler(scenario);

		controler.addOverridingModule(new BaselineModule());
		controler.addOverridingModule(new BaselineTransitModule());
		controler.addOverridingModule(new ZurichModule());
		//controler.addOverridingModule(new NewMultimodalModule());
		
		controler.addOverridingModule(new MultiModalModule());

		controler.run();
	}
}
