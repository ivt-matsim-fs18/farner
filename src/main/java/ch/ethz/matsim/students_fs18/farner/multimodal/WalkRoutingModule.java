package ch.ethz.matsim.students_fs18.farner.multimodal;

import java.util.List;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.router.DijkstraFactory;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.router.NetworkRoutingModule;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.facilities.Facility;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class WalkRoutingModule implements RoutingModule {
	final private Network walkNetwork;
	final private RoutingModule delegate;

	@Inject
	public WalkRoutingModule(@Named("walk") Network walkNetwork, WalkTravelTime walkTravelTime, Population population) {
		LeastCostPathCalculator router = new DijkstraFactory().createPathCalculator(walkNetwork,
				new OnlyTimeDependentTravelDisutility(walkTravelTime), walkTravelTime);

		this.delegate = new NetworkRoutingModule("walk", population.getFactory(), walkNetwork, router);
		this.walkNetwork = walkNetwork;
	}

	@Override
	public List<? extends PlanElement> calcRoute(Facility<?> fromFacility, Facility<?> toFacility, double departureTime,
			Person person) {
		Link fromLink = NetworkUtils.getNearestLink(walkNetwork, fromFacility.getCoord());
		Link toLink = NetworkUtils.getNearestLink(walkNetwork, toFacility.getCoord());

		Facility<?> newFromFacility = new LinkWrapperFacility(fromLink);
		Facility<?> newToFacility = new LinkWrapperFacility(toLink);

		return delegate.calcRoute(newFromFacility, newToFacility, departureTime, person);
	}

	@Override
	public StageActivityTypes getStageActivityTypes() {
		return delegate.getStageActivityTypes();
	}
}
