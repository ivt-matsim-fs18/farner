package org.matsim.contrib.multimodal.simengine;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.contrib.multimodal.MultiModalControlerListener;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.PlansCalcRouteConfigGroup;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.router.DijkstraFactory;
import org.matsim.core.router.NetworkRoutingModule;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.TravelTime;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.students_fs18.farner.multimodal.WalkTravelTime;

public class NewMultimodalModule extends AbstractModule {
	/*
	 * @Override public void install() {
	 * addRoutingModuleBinding("walk").to(WalkRoutingModule.class);
	 * bind(WalkTravelTime.class); }
	 * 
	 * @Singleton
	 * 
	 * @Named("walk")
	 * 
	 * @Provides public Network provideWalkNetwork(Network fullNetwork) { Network
	 * walkNetwork = NetworkUtils.createNetwork(); new
	 * TransportModeNetworkFilter(fullNetwork).filter(walkNetwork,
	 * Collections.singleton("car")); return walkNetwork; }
	 */

	public void install() {
		addControlerListenerBinding().to(MultiModalControlerListener.class);
	}

	public Collection<AbstractQSimPlugin> provideQSimModules(Config config) {
		List<AbstractQSimPlugin> plugins = new LinkedList<>(new BaselineTransitModule().provideQSimPlugins(config));
		plugins.add(new NewMultimodalQSimPlugin(config));
		return plugins;
	}

	@Singleton
	@Named("walk")
	@Provides
	public TravelTime provideWalkTravelTime(PlansCalcRouteConfigGroup plansCalcRouteConfigGroup, Network network) {
		/*
		 * Map<Id<Link>, Double> linkSlopes = new HashMap<>();
		 * 
		 * for (Link link : network.getLinks().values()) { linkSlopes.put(link.getId(),
		 * 0.0); }
		 * 
		 * return new WalkTravelTimeFactory(plansCalcRouteConfigGroup,
		 * linkSlopes).get();
		 */

		return new WalkTravelTime();
	}

	@Singleton
	@Named("walk")
	@Provides
	public Network provideWalkNetwork(Network fullNetwork) {
		Network walkNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(fullNetwork).filter(walkNetwork, Collections.singleton("car"));
		return walkNetwork;
	}

	@Singleton
	@Named("walk")
	@Provides
	public RoutingModule provideWalkRoutingModule(@Named("walk") Network walkNetwork, Population population,
			@Named("walk") TravelTime walkTravelTime) {
		LeastCostPathCalculator router = new DijkstraFactory().createPathCalculator(walkNetwork,
				new OnlyTimeDependentTravelDisutility(walkTravelTime), walkTravelTime);

		return new NetworkRoutingModule("walk", population.getFactory(), walkNetwork, router);
	}
}
